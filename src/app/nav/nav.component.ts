import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.Component.css']
})
export class NavComponent {
  constructor(private modalService: NgbModal) {
  }
  
  public open(modal: any): void {
    this.modalService.open(modal);
  }
}
